/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e3;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejercicio0202
{
  public static void main(String[] arg)
  {
    boolean a,b;
    a = true;
    b = true;
     
    System.out.println("Tabla de verdad de operadores logicos\n");
    System.out.println("A     B    NOT A   A AND B   A OR B   A XOR B");
     
    System.out.println(a+" "+b+"  "+!a+"   "+(a && b)+" \t"+(a || b)+" "
    +(a ^ b));
    a=false;
    b=true;
    System.out.println(a+" "+b+"  "+!a+"   "+(a && b)+" \t"+(a || b)+" "
    +(a ^ b));
	a=true;
	b=false;
    System.out.println(a+" "+b+"  "+!a+"   "+(a && b)+" \t"+(a || b)+" "
    +(a ^ b));
	a=false;
	b=false;
    System.out.println(a+" "+b+"  "+!a+"   "+(a && b)+" \t"+(a || b)+" "
    +(a ^ b));

  }
}
