/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e3;

/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-oct-2013
 */
public class Ejercicio0209 {

    public static void main(String[] args) {
        double radio = 5.2;
        final double PI = 3.141592;
        System.out.println(PI * radio * radio);
    }
}

/* EJECUCION:
84.94864768000001
*/